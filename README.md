# VenenuX minetest 0.4 mod unifieddyes

This mod provides complete set of collors for mod authors.. 
this is a 0.4.17 branch hack. For 5.2.X se other branchs

## About 

VanessaE's Unified Dyes - The purpose of this mod originally 
was to supply a complete set of colors for Minetest mod authors 
to use for colorized nodes, or to reference in recipes. 
Since the advent of the default dyes mod in minetest_game, 
this mod has become more of an extension of the default mod.  
Since the advent of param2 colorization, it has also become a 
library for general color handling.

Unified Dyes expands the standard dye set from 15 colors 
to 32, 89, or 256 (see the API and usage info).

## Usage

Documentation was originally posted at [the Unified Dyes Thread](https://forum.minetest.net/viewtopic.php?f=11&t=2178&p=28399) on the Minetest forum.

Our documentation is at [https://codeberg.org/venenux/venenux-minetest/src/branch/master/mods/unifieddyes/unifieddyes-en.md](https://codeberg.org/venenux/venenux-minetest/src/branch/master/mods/unifieddyes/unifieddyes-en.md)

API: the full API is the [API.md](API.md) file.

## Download: 

* mientest 5.4 -> https://gitlab.com/VanessaE/unifieddyes
* mientest 5.2 -> https://codeberg.org/minenux/minetest-mod-unifieddyes/archive/5.2.0.tar.gz
* mientest 0.4 -> https://codeberg.org/minenux/minetest-mod-unifieddyes/archive/0.4.17.tar.gz

## Dependencies: 

dyes from basic games, basic_materials; this is a repo for 0.4.17 api backports hacks

## install

Clone in your mods directory or in global mods directory adn restart the engine:

```
git clone https://codeberg.org/minenux/minetest-mod-unifieddyes  unifieddyes
```

## License: 

LGPL 3.0 for code, CC-by-SA 4.0 for media and everything else.

